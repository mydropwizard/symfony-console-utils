Symfony Console Utils
=====================

Some small utility classes for Symfony Console apps.

For a complete template using them see
[mydropwizard/symfony-console-project](https://gitlab.com/mydropwizard/symfony-console-template).

