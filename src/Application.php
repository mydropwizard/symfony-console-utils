<?php

namespace myDropWizard\ConsoleUtils;

use Symfony\Component\Console\Application as BaseApplication;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Console application class.
 */
class Application extends BaseApplication {

  /**
   * The path to application code.
   *
   * @var string
   */
  protected $appRoot;

  /**
   * The path the application was run from.
   *
   * @var string
   */
  protected $localRoot;

  /**
   * The container.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  private $container;

  /**
   * Constructs an Application.
   *
   * @param string $name
   *   The application name.
   * @param string $version
   *   The application version.
   * @param string $app_root
   *   The root of the application.
   * @param string $local_root
   *   The root of where the user is running the application.
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   */
  public function __construct($name, $version, $app_root, $local_root, ContainerInterface $container) {
    parent::__construct($name, $version);

    $this->appRoot = $app_root;
    $this->localRoot = $local_root;
    $this->container = $container;
  }

  /**
   * Gets the application container.
   *
   * @return \Symfony\Component\DependencyInjection\ContainerInterface
   *   The container.
   */
  public function getContainer() {
    return $this->container;
  }

  /**
   * Get the path to the application code.
   *
   * @return string
   *   The path to the application code.
   */
  public function getAppRoot() {
    return $this->appRoot;
  }

  /**
   * Get the path where the application is being run.
   *
   * @return string
   *   The path where the application is being run.
   */
  public function getLocalRoot() {
    return $this->localRoot;
  }

  /**
   * Change the directory where the application is being run.
   *
   * @param string $dir
   *   The directory (relative to the current directory) to use.
   *
   * @return string
   *   The new local root.
   *
   * @see ::getLocalRoot()
   */
  public function chdir($dir) {
    chdir($dir);
    $cwd = getcwd();
    if (!$cwd) {
      throw new RuntimeException("Unable to determine current working directory");
    }
    $this->localRoot = $cwd;
    return $this->localRoot;
  }

}
