<?php

namespace myDropWizard\ConsoleUtils;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\EventDispatcher\DependencyInjection\RegisterListenersPass;

/**
 * Bootstraps the application.
 */
abstract class Bootstrapper {

  /**
   * The absolute path to the application code.
   *
   * @var string
   */
  protected $appRoot;

  /**
   * The absolute path to the directory the app was run from.
   *
   * @var string
   */
  protected $localRoot;

  /**
   * Bootstrapper constructor.
   *
   * @param string $app_root
   *   The absolute path to the application code.
   * @param string $local_root
   *   The absolute path to the directory the app was run from.
   */
  public function __construct($app_root, $local_root) {
    $this->appRoot = $app_root;
    $this->localRoot = $local_root;
  }

  /**
   * Create the application object.
   *
   * @param string $app_root
   *   The app root.
   * @param string $local_root
   *   The local root.
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   *
   * @return \myDropWizard\ConsoleUtils\Application
   *   The application object.
   */
  abstract protected function createApplication($app_root, $local_root, ContainerInterface $container);

  /**
   * Bootstraps the application.
   *
   * @return \myDropWizard\ConsoleUtils\Application
   *   The fully bootstrapped application object.
   *
   * @throws \Exception
   *   If unable to successfully bootstrap.
   */
  public function bootstrap() {
    $container = $this->buildContainer();

    $application = $this->createApplication($this->appRoot, $this->localRoot, $container);
    $container->set('application', $application);

    $container->compile();

    /** @var \Symfony\Component\EventDispatcher\EventDispatcher $event_dispatcher */
    $event_dispatcher = $container->get('event_dispatcher');
    $application->setDispatcher($event_dispatcher);

    /** @var \myDropWizard\ConsoleUtils\CommandManager $command_manager */
    $command_manager = $container->get('command_manager');
    foreach ($command_manager->getCommands() as $command) {
      $application->add($command);
    }

    return $application;
  }

  /**
   * Builds the application container.
   *
   * @return \Symfony\Component\DependencyInjection\ContainerBuilder
   *   The container builder.
   *
   * @throws \Exception
   *   If there's issues building the container.
   */
  protected function buildContainer() {
    $container = new ContainerBuilder();

    $default_loader = new YamlFileLoader($container, new FileLocator($this->appRoot));
    $default_loader->load('services.yml');

    $container->set('container', $container);

    $container->addCompilerPass(new RegisterListenersPass('event_dispatcher', 'event_listener', 'event_subscriber'));
    $container->addCompilerPass(new CommandCompilerPass());

    return $container;
  }

}
