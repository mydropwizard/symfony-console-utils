<?php

namespace myDropWizard\ConsoleUtils;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manages the list of commands.
 */
class CommandManager {

  /**
   * The container.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected $container;

  /**
   * An associative array with service ids as keys, with options as values.
   *
   * @var array
   */
  protected $commands = [];

  /**
   * Constructs the command manager.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   */
  public function __construct(ContainerInterface $container) {
    $this->container = $container;
  }

  /**
   * Registers a command.
   *
   * @param string $id
   *   The id on the container.
   * @param array $options
   *   An array of options.
   */
  public function registerCommand($id, array $options = []) {
    $this->commands[$id] = $options;
  }

  /**
   * Gets the command objects that apply to this environment.
   *
   * @return \Symfony\Component\Console\Command\Command[]
   *   The command objects.
   */
  public function getCommands() {
    $commands = [];

    foreach ($this->commands as $command_id => $options) {
      $command = $this->container->get($command_id);
      if ($command instanceof Command) {
        $commands[] = $command;
      }
      else {
        throw new LogicException("Service $command_id marked as command, but doesn't implement Command class");
      }
    }

    return $commands;
  }

}
