<?php

namespace myDropWizard\ConsoleUtils\EventListeners;

use Monolog\Handler\PsrHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Event listener to do any pre-launch operations before running a command.
 */
class CommandPrelaunch {

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * CommandPrelaunch constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(LoggerInterface $logger) {
    $this->logger = $logger;
  }

  /**
   * Responds to the console command event.
   *
   * @param \Symfony\Component\Console\Event\ConsoleCommandEvent $event
   *   The console command event.
   */
  public function onEvent(ConsoleCommandEvent $event) {
    // Configure the logger.
    if ($this->logger instanceof Logger) {
      $this->logger->pushHandler(new PsrHandler($this->createConsoleLogger($event->getOutput())));
    }
  }

  /**
   * Creates a console logger with the right verbosity.
   *
   * @param \Symfony\Component\Console\Output\OutputInterface $output
   *   The console output.
   *
   * @return \Symfony\Component\Console\Logger\ConsoleLogger
   *   The new console logger.
   */
  protected function createConsoleLogger(OutputInterface $output) {
    return new ConsoleLogger($output, [
      LogLevel::NOTICE => OutputInterface::VERBOSITY_NORMAL,
      LogLevel::INFO => OutputInterface::VERBOSITY_VERBOSE,
      LogLevel::DEBUG => OutputInterface::VERBOSITY_DEBUG,
    ]);
  }

}
