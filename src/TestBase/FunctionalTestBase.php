<?php

namespace myDropWizard\ConsoleUtils\TestBase;

/**
 * Base class for functional tests.
 */
abstract class FunctionalTestBase extends \PHPUnit_Framework_TestCase {

  /**
   * Paths to temporary directories.
   *
   * @var string[]
   */
  protected $tempDirectories;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->tempDirectories = [];
  }

  /**
   * Gets a random string.
   *
   * @param int $length
   *   The number of characters in the string.
   *
   * @return string
   *   A random string.
   */
  protected function getRandomString($length = 7) {
    return substr(md5((string) rand()), 0, $length);
  }

  /**
   * Creates a unique temporary directory we can use for our tests.
   *
   * @return string
   *   Path to a temporary directory.
   */
  protected function createTemporaryDirectory() {
    $temp_root = sys_get_temp_dir();

    do {
      $tempdir = $temp_root . '/test-' . $this->getRandomString(7);
    } while (file_exists($tempdir));

    mkdir($tempdir, 0755, TRUE);

    $this->tempDirectories[] = $tempdir;

    return $tempdir;
  }

  /**
   * {@inheritdoc}
   */
  protected function tearDown() {
    parent::tearDown();

    $preserve_environment = getenv('TEST_PRESERVE_ENVIRONMENT');
    if (empty($preserve_environment)) {
      // Clean-up temporary directories.
      foreach ($this->tempDirectories as $tempdir) {
        shell_exec("rm -rf {$tempdir}");
      }
    }

    $this->tempDirectories = [];
  }

  /**
   * {@inheritdoc}
   */
  protected function onNotSuccessfulTest(\Exception $e) {
    try {
      $this->tearDown();
    }
    catch (\Exception $e2) {
      print "** Exception caught while trying to clean-up after failed test:\n";
      print $e2->getMessage() . "\n";
    }

    parent::onNotSuccessfulTest($e);
  }

}
