<?php

namespace myDropWizard\ConsoleUtils\TestBase;

/**
 * Base class for unit tests.
 */
abstract class UnitTestBase extends \PHPUnit_Framework_TestCase {

  /**
   * Call protected/private method of a class.
   *
   * @param object $object
   *   Instantiated object that we will run method on.
   * @param string $methodName
   *   Method name to call.
   * @param array $parameters
   *   Array of parameters to pass into method.
   *
   * @return mixed
   *   Method return.
   */
  public function invokeMethod(&$object, $methodName, array $parameters = []) {

    $reflection = new \ReflectionClass(get_class($object));
    $method = $reflection->getMethod($methodName);
    $method->setAccessible(TRUE);

    return $method->invokeArgs($object, $parameters);
  }

}
