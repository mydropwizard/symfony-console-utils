<?php

namespace myDropWizard\ConsoleUtils;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Collects commands in a compiler pass and adds them to the manager.
 */
class CommandCompilerPass implements CompilerPassInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContainerBuilder $container) {
    if (!$container->has('command_manager')) {
      return;
    }

    /** @var \myDropWizard\ConsoleUtils\CommandManager $manager */
    $manager = $container->get('command_manager');

    $taggedServices = $container->findTaggedServiceIds('console.command');
    foreach ($taggedServices as $id => $tags) {
      $manager->registerCommand($id, $tags[0]);
    }
  }

}
